from src.pipe.pipeline import Pipeline
import pandas as pd
from config.config import Config
import os

# Script
def main():
    pipeline = Pipeline('Insert An ICAO')
    settings = Config('./config/config.yml').get('settings')
    pipeline.Run(settings)


if __name__ == '__main__':
    main()